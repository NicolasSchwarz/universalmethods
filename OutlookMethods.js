/*
GENERATE OUTLOOK-CALENDAR-LINK

EXAMPLE DATA
var startDate="2023-01-12";
var startTimeHour="18";
var startTimeMinute="01";
var endDate="2023-01-13";
var endTimeHour= "19";
var endTimeMinute ="44";
var title = "Test Titel";
var description = "TestDescrption";
var location ="Los Angeles";
*/

//Method as a const-lambda 
const getOutlookCalendarLink = (startDate, startTimeHour, startTimeMinute, endDate, endTimeHour, endTimeMinute, title, description, location) => {
    
	const startTimeHourFinal=startTimeHour-1;
	const endTimeHourFinal= endTimeHour-1;
 
    const link = "https://outlook.office.com/calendar/0/deeplink/compose?body=" + 
        description + "&enddt="+endDate+ "T"+endTimeHourFinal+"%3A"+ endTimeMinute+
        "%3A00%2B00%3A00&location="+ location + 
        "&path=%2Fcalendar%2Faction%2Fcompose&rru=addevent&startdt="+
        startDate+"T"+startTimeHourFinal+"%3A"+startTimeMinute+
        "%3A00%2B00%3A00&subject="+title;

    return link;
};

//Method as a good old function
function getOutlookCalendarLink(startDate, startTimeHour, startTimeMinute, endDate, endTimeHour, endTimeMinute, title, description, location){
    
    const startTimeHourFinal=startTimeHour-1;
	const endTimeHourFinal= endTimeHour-1;
 
    const link = "https://outlook.office.com/calendar/0/deeplink/compose?body=" + 
        description + "&enddt="+endDate+ "T"+endTimeHourFinal+"%3A"+ endTimeMinute+
        "%3A00%2B00%3A00&location="+ location + 
        "&path=%2Fcalendar%2Faction%2Fcompose&rru=addevent&startdt="+
        startDate+"T"+startTimeHourFinal+"%3A"+startTimeMinute+
        "%3A00%2B00%3A00&subject="+title;

    return link;
};