//ALL METHODS ARE GIVING BACK A CODE-MESSAGE JSON


passwordValidator = (passwd) => {

    const numbers = /^\d+$/;
    const specialChars = /^[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]+$/;
    const lowerCaseChars = /[a-z]/;
    const upperCaseChars = /[A-Z]/;
    
    let response = {"code":200,"msg":""};

    if(passwd.length<7 || passwd.length>15){
        response.code = 409;
        response.msg = "Das Passwort muss mindestens 7 und darf maximal 15 Zeichen haben";
    }

    if(passwd.search(numbers) <1){
        response.code = 409;
        response.msg = "Das Passwort muss Zahlen enthalten";
    }

    if (passwd.search(lowerCaseChars) < 1 || passwd.search(upperCaseChars) < 1) {
        response.code = 409;
        response.msg = "Das Passwort muss Groß- und Kleinbuchstaben enthalten";
    }

    if(passwd.search(specialChars) <1){
        response.code = 409;
        response.msg = "Das Passwort Sonderzeichen enthalten";
    }

    return response;
}


emailValidator = (email) => {

    let response = {"code":200,"msg":""};

    if (!/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email)){
        response.code = 409;
        response.msg = "Die Email-Adresse ist nicht valide.";
    }  
    
    return response;
}